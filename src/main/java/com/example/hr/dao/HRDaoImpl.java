package com.example.hr.dao;

import com.example.hr.models.Employee;
import com.example.hr.models.SearchCriteria;
import com.example.hr.utils.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HRDaoImpl implements HRDao {
    @Override
    public Employee saveEmployee(Employee emp) {
        return null;
    }

    @Override
    public Employee updateEmployee(Employee emp) {
        return null;
    }

    @Override
    public Employee deleteEmployee(int empID) {
        return null;
    }

    @Override
    public List<Employee> getAllEmployee() {

        ResultSet rs = null;
        PreparedStatement pst = null;
        Connection con = DBConnection.getConnection();
        String stm = "Select * from employee";
        List<Employee> records = new ArrayList<Employee>();

        try {
            pst = con.prepareStatement(stm);
            pst.execute();
            rs = pst.getResultSet();

            while (rs.next()) {
                Employee emloyee = new Employee();
                emloyee.setID(rs.getInt(1));
                emloyee.setFirstName(rs.getString(2));
                emloyee.setLastName(rs.getString(3));
                emloyee.setDivision(rs.getString(4));
                emloyee.setBuilding(rs.getString(5));
                emloyee.setTitle(rs.getString(6));
                emloyee.setRoom(rs.getString(7));
                records.add(emloyee);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return records;
    }

    @Override
    public List<Employee> searchEmployee(SearchCriteria criteria) {
        return null;
    }
}
