package com.example.hr.service;

import com.example.hr.dao.HRDao;
import com.example.hr.dao.HRDaoImpl;
import com.example.hr.models.Employee;
import com.example.hr.models.SearchCriteria;
import lombok.Setter;

import java.util.List;

public class HRServiceImpl implements HRService {

    @Setter
    HRDao hrDao;

    {
        hrDao = new HRDaoImpl();
    }

    @Override
    public Employee saveEmployee(Employee emp) {
        return null;
    }

    @Override
    public Employee updateEmployee(Employee emp) {
        return null;
    }

    @Override
    public Employee deleteEmployee(int empID) {
        return null;
    }

    @Override
    public List<Employee> getAllEmployee() {
        return hrDao.getAllEmployee();
    }

    @Override
    public List<Employee> searchEmployee(SearchCriteria criteria) {
        return null;
    }
}
