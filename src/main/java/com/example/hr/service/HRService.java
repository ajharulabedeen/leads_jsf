/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.hr.service;

import com.example.hr.models.Employee;
import com.example.hr.models.SearchCriteria;

import java.util.List;

public interface HRService {

    public Employee saveEmployee(Employee emp);

    public Employee updateEmployee(Employee emp);

    public Employee deleteEmployee(int empID);

    public List<Employee> getAllEmployee();

    public List<Employee> searchEmployee(SearchCriteria criteria);
}
