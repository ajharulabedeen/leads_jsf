package com.example.hr.dao;

import com.example.hr.models.Employee;
import com.example.hr.models.SearchCriteria;

import java.util.List;

public interface HRDao {
    public Employee saveEmployee(Employee emp);

    public Employee updateEmployee(Employee emp);

    public Employee deleteEmployee(int empID);

    public List<Employee> getAllEmployee();

    public List<Employee> searchEmployee(SearchCriteria criteria);
}
