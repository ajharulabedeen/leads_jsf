package com.example.hr.employee;

import com.example.hr.models.Employee;
import com.example.hr.service.HRService;
import lombok.Setter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;
import lombok.Getter;

@ManagedBean(name = "employee", eager = true)
@SessionScoped
public class EmployeeBean {

    private HRService hrService;
    
    private int searchId;

    public int getSearchId() {
        return searchId;
    }

    public void setSearchId(int searchId) {
        this.searchId = searchId;
    }

    public void setHrService(HRService hrService) {
        this.hrService = hrService;
    }

    public List<Employee> getAllEmployee() {
        return hrService.getAllEmployee();
    }

    public void getEmployeeSearch() {
        System.out.println("getEmployeeSearch");
    }

    public void getPrint() {
        System.out.println("Method");
    }
}
